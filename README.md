## Initialization

1. Copy config.ini file from setup folder to root folder.
2. In config.ini in root folder:
	a. Replace {philips-hue-api-user-key} with your userid key from Hue api.
	b. Replace {hue-hub-ip-address} with the IP address of your Philips Hue hub.

3. pip3 install python-crontab pytz

---

## Using the script

Using the script is easy. 
Just call it like 'python3 lights.py {comma-separated-list-of-light-ids} {comma-separated-list-of-light-attributes}'

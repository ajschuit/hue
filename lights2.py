#!/usr/bin/env python3
import sys,configparser,requests,re,datetime,json


print("Starting run at "+str(datetime.datetime.now()))

config = configparser.ConfigParser()
config.read('config.ini')

pat = re.compile('\{(\"[a-z_]+\":(true|false|\"select\"|\"lselect\"|\"none\"|\"colorloop\"|-?\d+),)*(\"[a-z]+\":(true|false|\"select\"|\"lselect\"|\"none\"|\"colorloop\"|-?\d+))\}')

hue_user = config['DEFAULT']['HUE_USER']
ip_addr = config['DEFAULT']['IP_ADDR']

base_url = 'http://'+ip_addr+'/api/'+hue_user+'/'

larg = len(sys.argv)
if larg < 3 or larg%2 == 0:
 print("Incorrect usage. Received {} arguments.".format(larg))
 exit()
i=1
while i < larg:
 lights = sys.argv[i]
# data = sys.argv[i+1]
# match = pat.fullmatch(data)
# if not match:
#  print("Incorrect data format: "+data)
#  exit();
# for light in lights.split(','):
#  if light[0:1] == 'g':
#   url = base_url+'groups/'+light[1:]+'/action'
#  else:
#   url = base_url+'lights/'+light+'/state'
#  r = requests.put(url,data)
#  print(r.content)
 i+=2
for light in lights.split(','):
 if light[0:1] == 'g':
  url = base_url+'groups/'+light[1:]
 else:
  url = base_url+'lights/'+light
 r = requests.get(url)
 d = json.loads(r.content)
 print(d["state"])


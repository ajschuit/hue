#!/usr/bin/env python3
import requests,pytz,json,random
from crontab import CronTab
from datetime import datetime,timedelta

#set your latitude and longitude here as strings
lat = '42.898076'
lon = '-85.748097'

r = requests.get('https://api.sunrise-sunset.org/json?lat='+lat+'&lng='+lon+'&formatted=0')
data = json.loads(r.content.decode('UTF-8'))
if data['status']!='OK':
 print("Error loading sunrise/sunset data: "+json.dumps(data))
 exit()

sunrise = datetime.strptime(data['results']['sunrise'],'%Y-%m-%dT%H:%M:%S+00:00')
sunset = datetime.strptime(data['results']['sunset'],'%Y-%m-%dT%H:%M:%S+00:00')

sunrise = pytz.utc.localize(sunrise).astimezone()
sunset = pytz.utc.localize(sunset).astimezone()



#add some randomness for porch and bedroom
porchoff = sunrise + timedelta(minutes=random.randint(1,15))

#set minimum "sunrise" time for bedroom
if sunrise.hour < 9:
 sunrise = sunrise.replace(hour=9, minute=0)

bedoff = sunrise + timedelta(minutes=random.randint(-5,5))

#from this point on, will be different depending on what your crontab looks like
cron = CronTab(user='pi')

for job in cron.find_comment('morning bedroom off'):
 job.hour.on(bedoff.hour)
 job.minute.on(bedoff.minute)

for job in cron.find_comment('morning porch off'):
 job.hour.on(porchoff.hour)
 job.minute.on(porchoff.minute)

for job in cron.find_comment('night porch on'):
 job.hour.on(sunset.hour)
 job.minute.on(sunset.minute)

cron.write()
